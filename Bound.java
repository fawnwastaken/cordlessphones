import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.lang.Math;

public class Bound{

   static double e=.000000000001; // for errors  
    // returns all points that are on the boudary of the cicle.
    public static   List<Point> bounds(List<Point> points ,Circle circle){
        List<Point> bounding=new ArrayList<Point>();
        double r=circle.r; // radius of circle
        // System.out.println(circle.r);
        Point origin=circle.c;
        // checks all points in circle and finds the ones on boundary
        for(int i=0;i<points.size();i++){
            if(findDist(points.get(i),origin)>=(r-e)){ 
                bounding.add(points.get(i));


            }


        }
        //for(int j=0;j<bounding.size();j++){
        //    System.out.println(bounding.get(j));
        //}
        return bounding;
    }



    public static  double findDist(Point a, Point b){
        double x1=a.x;
        double y1=a.y;
        double x2=b.x;
        double y2=b.y;

        double x=x1-x2;
        double y=y1-y2;
        //System.out.println(Math.sqrt(Math.pow(x,2)+Math.pow(y,2)));
        return(Math.sqrt(Math.pow(x,2)+Math.pow(y,2)));
       
    }

    // returns the new list of points with the largest point removed
    public static  List<Point> removePoint(List<Point> boundedPoints,List<Point> pointsinCircle, int numberLeft){
        //if your list of points is smaller than the amount your want it reduces or the same then your done
  
        
        
        Point largestPoint=null;
        Point removed=null;
        double largestDistance=1000000;
        while(boundedPoints.size()>0){
            // removed the first elemnt of bounded
            removed=boundedPoints.get(0);
            boundedPoints.remove(0);
            // removes point from points we are going to get smallest from
            for(int j=0;j<pointsinCircle.size();j++){
                if(pointsinCircle.get(j).equals(removed)){
                    pointsinCircle.remove(j);
                    //System.out.println("removied a point in remove pointmethod"+removed);
                }
            }
            Circle circle=SmallestEnclosingCircle.makeCircle(pointsinCircle);
                // found largest radius out to date so make record it
                if(circle.r<largestDistance){
                    largestDistance=circle.r;
                
                    //System.out.println(circle);
                    largestPoint=removed;
                    //System.out.println(removed);
                    //System.out.println("Current outer point found");
                    
                }
                    
                // puts the point previously removed back in pointlist so we can look with other bounded
                //System.out.println("removed "+removed);
                pointsinCircle.add(removed);
              

        }
        //System.out.println("largest point that should be removed is "+largestPoint);
        // now return a new set of points with the largest removed
        for(int z=0;z<pointsinCircle.size();z++){
            //System.out.println("points that are still in the circle are "+(pointsinCircle.get(z)));
            Point t=pointsinCircle.get(z);
           
            if(t.equals(largestPoint)){
            pointsinCircle.remove(z);
               
            }
        }
            for(int o=0;o<pointsinCircle.size();o++){
                //System.out.println("final"+pointsinCircle.get(o));

        }
        return pointsinCircle;


    }





    // given a set of points and how many to contain in end will give the smallest circle 
    public static  Circle findXAmountPoints(List<Point> pointArray,int numLeft){
        //System.out.println("here");
        // removes points until down to required size
        List<Point> bounding=null;
        while (pointArray.size()>numLeft){
         
            bounding=bounds(pointArray,SmallestEnclosingCircle.makeCircle(pointArray));
         
            pointArray=removePoint(bounding,pointArray,numLeft);
           

        }

       return  SmallestEnclosingCircle.makeCircle(pointArray);



    }

    // testing for special cases yet to be figured out though
    public static boolean SpecialCases(List<Point> bounds,List<Point> pointArray,int numLeft){






        return true;

    }

}
